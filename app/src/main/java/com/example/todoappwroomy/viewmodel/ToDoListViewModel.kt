package com.example.todoappwroomy.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.todoappwroomy.model.RepoImpl
import com.example.todoappwroomy.model.apimodel.ToDo
import com.example.todoappwroomy.util.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ToDoListViewModel(val repo: RepoImpl): ViewModel() {

    private var _isChecked: MutableLiveData<Boolean> = MutableLiveData<Boolean>(false)
    val isChecked: LiveData<Boolean> get() = _isChecked

    private var _todos = MutableLiveData<Resource<List<ToDo>>>(Resource.Loading())
    val todos: LiveData<Resource<List<ToDo>>> get() = _todos

    fun getAllTodos() = viewModelScope.launch(Dispatchers.Main) {
        val response = repo.grabAllToDos()
        _todos.value = response
    }
    fun IsChecked(status: Boolean) {
        _isChecked.value = status
    }
}