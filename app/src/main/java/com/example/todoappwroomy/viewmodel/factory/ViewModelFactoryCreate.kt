package com.example.todoappwroomy.viewmodel.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.todoappwroomy.model.RepoImpl
import com.example.todoappwroomy.viewmodel.CreateToDoListViewModel

class ViewModelFactoryCreate(
    private val repo: RepoImpl): ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return CreateToDoListViewModel(repo) as T
    }
}