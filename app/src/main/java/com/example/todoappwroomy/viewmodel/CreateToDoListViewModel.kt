package com.example.todoappwroomy.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.todoappwroomy.model.RepoImpl
import com.example.todoappwroomy.model.apimodel.ToDo
import com.example.todoappwroomy.util.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class CreateToDoListViewModel(val repo: RepoImpl): ViewModel() {
    var title: String = ""
    var description: String = ""

    private var _continueButton = MutableLiveData<Boolean>(false)
    val continueButton: LiveData<Boolean> get() = _continueButton

    fun setContinueBtnValue(flag: Boolean){
        _continueButton.value = flag
    }
    fun addToDoItem(todo: ToDo) = viewModelScope.launch(Dispatchers.Main) {
        repo.updateTodo(todo)
    }
}