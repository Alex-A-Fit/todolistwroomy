package com.example.todoappwroomy.viewmodel.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.todoappwroomy.model.RepoImpl
import com.example.todoappwroomy.viewmodel.CreateToDoListViewModel
import com.example.todoappwroomy.viewmodel.ToDoListViewModel

class ViewModelFactoryToDoList(
    private val repo: RepoImpl): ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ToDoListViewModel(repo) as T
    }
}