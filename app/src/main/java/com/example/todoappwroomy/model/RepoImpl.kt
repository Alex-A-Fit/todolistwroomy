package com.example.todoappwroomy.model

import android.content.Context
import com.example.todoappwroomy.model.apimodel.ToDo
import com.example.todoappwroomy.model.room.ToDoDatabase
import com.example.todoappwroomy.util.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.lang.Exception

class RepoImpl(context: Context): Repo {

    private val todoDao = ToDoDatabase.getDatabaseInstance(context).todoDao()

    override suspend fun updateTodo(todo: ToDo) = withContext(Dispatchers.IO) {
        todoDao.addToDoItem(todo)
    }

    override suspend fun grabAllToDos(): Resource<List<ToDo>> = withContext(Dispatchers.IO){
        return@withContext try {
            val response = todoDao.grabAllToDos()
            Resource.Success(response)
        } catch (e: Exception) {
            Resource.Error(e.localizedMessage ?: "Something went wrong")
        }
    }

}