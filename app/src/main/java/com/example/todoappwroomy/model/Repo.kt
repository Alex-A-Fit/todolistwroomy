package com.example.todoappwroomy.model

import com.example.todoappwroomy.model.apimodel.ToDo
import com.example.todoappwroomy.util.Resource

interface Repo {
    suspend fun updateTodo(todo: ToDo)
    suspend fun grabAllToDos(): Resource<List<ToDo>>
}