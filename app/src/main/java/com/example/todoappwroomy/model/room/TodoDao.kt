package com.example.todoappwroomy.model.room

import androidx.room.*
import com.example.todoappwroomy.model.apimodel.ToDo
import com.example.todoappwroomy.util.Resource

@Dao
interface TodoDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addToDoItem(todo: ToDo)

    @Query("SELECT * FROM todo_list")
    fun  grabAllToDos(): List<ToDo>

    @Delete
    suspend fun deleteToDoItem(todo: ToDo)
}