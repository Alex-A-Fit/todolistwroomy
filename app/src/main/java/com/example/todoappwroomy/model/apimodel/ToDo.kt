package com.example.todoappwroomy.model.apimodel

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

@Entity(tableName = "todo_list")
@Parcelize
data class ToDo(
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    val title: String = "",
    val description: String = "",
    val completed: Boolean,
    val date: String ="",
    val updated: String =""
): Parcelable
