package com.example.todoappwroomy.model.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.todoappwroomy.model.apimodel.ToDo

@Database(entities = [ToDo::class], exportSchema = false, version=1)
abstract class ToDoDatabase: RoomDatabase() {
    abstract fun todoDao(): TodoDao

    companion object {
        const val DB_NAME = "TODO_DB"

        fun getDatabaseInstance(context: Context): ToDoDatabase{
            return Room.databaseBuilder(context, ToDoDatabase::class.java, DB_NAME)
                .fallbackToDestructiveMigration().build()
        }
    }
}