package com.example.todoappwroomy.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.todoappwroomy.databinding.FragmentToDoListBinding
import com.example.todoappwroomy.model.RepoImpl
import com.example.todoappwroomy.model.apimodel.ToDo
import com.example.todoappwroomy.util.Resource
import com.example.todoappwroomy.util.SwipeGesture
import com.example.todoappwroomy.view.adapters.ToDoAdapters
import com.example.todoappwroomy.viewmodel.ToDoListViewModel
import com.example.todoappwroomy.viewmodel.factory.ViewModelFactoryToDoList

class ToDoListFragment : Fragment() {
    private var _binding: FragmentToDoListBinding? = null
    private val binding: FragmentToDoListBinding get() = _binding!!

    private val repoImp by lazy {
        RepoImpl(requireContext())
    }
    private val viewmodel by viewModels<ToDoListViewModel> {
        ViewModelFactoryToDoList(repoImp)
    }

    fun CreateSwipeFeature(todo: List<ToDo>, recycleerViewTodo: RecyclerView) {
        val adapter = ToDoAdapters(::navigateToDetails)
        val swipeGesture = object : SwipeGesture(adapter) {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {

                when (direction) {
                    ItemTouchHelper.LEFT -> {
                        adapter.deleteItem(viewHolder.adapterPosition, todo)
                    }
                }
            }
        }
        val touchGesture = ItemTouchHelper(swipeGesture)
        touchGesture.attachToRecyclerView(recycleerViewTodo)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentToDoListBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onInitListener()
        viewmodel.getAllTodos()
    }

    override fun onResume() {
        super.onResume()
        viewmodel.getAllTodos()
    }

    private fun onInitListener() = with(binding) {
        addTodoBtn.setOnClickListener {
            navigateToDetails(
                ToDo(
                    title = "",
                    description = "",
                    completed = false,
                    updated = "",
                    date = ""
                )
            )
        }

        recycleerViewTodo.layoutManager = LinearLayoutManager(context)
        viewmodel.todos.observe(viewLifecycleOwner) { viewState ->
            when (viewState) {
                is Resource.Error -> {
                    Toast.makeText(context, viewState.message, Toast.LENGTH_SHORT).show()
                }
                is Resource.Success -> {
                    recycleerViewTodo.adapter = ToDoAdapters(::navigateToDetails).apply {
                        applyItems(viewState.data)
                        CreateSwipeFeature(viewState.data, recycleerViewTodo)
                    }
                }
                is Resource.Loading -> {}
                else -> {}
            }
        }
    }

    private fun navigateToDetails(todo: ToDo) {
        val action = ToDoListFragmentDirections.actionToDoListFragmentToCreateToDoFragment(todo)
        findNavController().navigate(action)
    }

}

