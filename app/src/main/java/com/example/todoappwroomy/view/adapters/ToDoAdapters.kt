package com.example.todoappwroomy.view.adapters

import android.graphics.Paint
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.todoappwroomy.databinding.TodoItemBinding
import com.example.todoappwroomy.model.apimodel.ToDo
import com.example.todoappwroomy.util.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class ToDoAdapters(val navigateToDetails: (todo: ToDo) -> Unit): RecyclerView.Adapter<ToDoAdapters.ToDoViewHolder>() {

    lateinit var data: List<ToDo>

    class ToDoViewHolder(
        val binding: TodoItemBinding
        ): RecyclerView.ViewHolder(binding.root) {
            fun applyItem(todo: ToDo) {
                binding.todoNum.text = (adapterPosition + 1).toString()
                binding.todoItem.text = todo.title
            }
        }

    fun deleteItem(index: Int, currentList: List<ToDo>) {
        data = currentList
        val newList: MutableList<ToDo> = mutableListOf<ToDo>()
        for (i in 0..data.count()) {
            if (data[i] != data[index]){
                newList.add(data[i])
            }
        }
        data = newList
        notifyItemRemoved(index)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ToDoViewHolder {
    val binding = TodoItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ToDoViewHolder(binding).apply {
            binding.root.setOnClickListener{
                navigateToDetails(data[adapterPosition])
            }
            binding.checkBox.setOnClickListener{
                binding.todoItem.strike = binding.checkBox.isChecked
            }
        }
    }

    override fun onBindViewHolder(holder: ToDoViewHolder, position: Int) {
        val todoItem = data[position]
        holder.applyItem(todoItem)
    }

    override fun getItemCount(): Int {
        return data.size
    }
    fun applyItems(todos: List<ToDo>) {
        data = todos
    }

}

inline var TextView.strike: Boolean
    set(visible) {
        paintFlags = if (visible) paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
        else paintFlags and Paint.STRIKE_THRU_TEXT_FLAG.inv()
    }
    get() = paintFlags and Paint.STRIKE_THRU_TEXT_FLAG == Paint.STRIKE_THRU_TEXT_FLAG

