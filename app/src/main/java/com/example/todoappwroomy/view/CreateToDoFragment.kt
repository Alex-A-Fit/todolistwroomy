package com.example.todoappwroomy.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.todoappwroomy.R
import com.example.todoappwroomy.databinding.FragmentCreateToDoBinding
import com.example.todoappwroomy.databinding.FragmentToDoListBinding
import com.example.todoappwroomy.model.RepoImpl
import com.example.todoappwroomy.model.apimodel.ToDo
import com.example.todoappwroomy.viewmodel.CreateToDoListViewModel
import com.example.todoappwroomy.viewmodel.ToDoListViewModel
import com.example.todoappwroomy.viewmodel.factory.ViewModelFactoryCreate
import com.example.todoappwroomy.viewmodel.factory.ViewModelFactoryToDoList
import java.time.Clock
import java.time.Instant

class CreateToDoFragment : Fragment() {
    private var _binding: FragmentCreateToDoBinding? = null
    private val binding: FragmentCreateToDoBinding get() = _binding!!

    val repoImp by lazy {
        RepoImpl(requireContext())
    }
    val viewmodel by viewModels<CreateToDoListViewModel> {
        ViewModelFactoryCreate(repoImp)
    }

    val args by navArgs<CreateToDoFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentCreateToDoBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onInitListener()
        if (args.todoItem.title.isNotEmpty() && args.todoItem.description.isNotEmpty()){
            binding.todoDescription.setText(args.todoItem.description)
            binding.todoTitle.setText(args.todoItem.title)
        }
    }

    private fun onInitListener() = with(binding){
        viewmodel.continueButton.observe(viewLifecycleOwner) { turnOnOff ->
            createTodoBtn.isEnabled = turnOnOff
        }
        todoTitle.addTextChangedListener {
            viewmodel.title = it.toString()
            validateInputs()
        }
        todoDescription.addTextChangedListener {
            viewmodel.description = it.toString()
            validateInputs()
        }
        createTodoBtn.setOnClickListener {
            val clock: Clock = Clock.systemDefaultZone()
            val time = clock.instant().toString()
            val instantBefore = Instant.parse(time)
            if (args.todoItem.id > 0){
                val todo = ToDo(
                    id = args.todoItem.id,
                    title = viewmodel.title,
                    description = viewmodel.description,
                    completed = false,
                    updated = instantBefore.toString(),
                    date = instantBefore.toString()
                )
                viewmodel.addToDoItem(todo)
            }else{
            val todo = ToDo(
                title = viewmodel.title,
                description = viewmodel.description,
                completed = false,
                updated = instantBefore.toString(),
                date = instantBefore.toString()
            )
                viewmodel.addToDoItem(todo)
            }

            findNavController().navigateUp()
        }
    }

    private fun validateInputs() {
        if (viewmodel.title.isNotEmpty() && viewmodel.description.isNotEmpty()){
            viewmodel.setContinueBtnValue(true)
        }else{
            viewmodel.setContinueBtnValue(false)
        }
    }
}
